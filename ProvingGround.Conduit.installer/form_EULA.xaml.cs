﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace ProvingGround.Conduit.installer
{
    /// <summary>
    /// Interaction logic for form_EULA.xaml
    /// </summary>
    public partial class form_EULA : Window
    {
        private bool _accept;

        public bool Acceptance
        {
            get
            {
                return _accept;
            }
        }

        public form_EULA()
        {
            InitializeComponent();

            SetUpEula();
        }

        private void SetUpEula()
        {
            RichText_Eula.Selection.Load(new FileStream("PGEula.rtf", FileMode.Open), DataFormats.Rtf);
        }

        private void Button_Decline_Click(object sender, RoutedEventArgs e)
        {
            _accept = false;
            this.Close();
        }

        private void Button_Accept_Click(object sender, RoutedEventArgs e)
        {
            _accept = true;
            this.Close();
        }



    }
}
