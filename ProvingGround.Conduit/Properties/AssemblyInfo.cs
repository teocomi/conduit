﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Conduit")]
[assembly: AssemblyDescription("Grasshopper tools for creating data visualizations and heads up displays (HUDs)")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("PROVING GROUND")]
[assembly: AssemblyProduct("ProvingGround.Conduit")]
[assembly: AssemblyCopyright("Copyright ©  2016 PROVING GROUND LLC")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("44cd8fff-4c2d-42ff-8440-4d48291732e8")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2016.5.3.0")]
[assembly: AssemblyFileVersion("2016.5.3.0")]
